package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync"
	//	"time"
)

var muSignerMd5 = &sync.Mutex{}

// сюда писать код

func ExecutePipeline(jobs ...job) {
	//fmt.Printf("%v Running %v jobs\n", time.Now().String(), len(jobs))
	wg := &sync.WaitGroup{}
	stdinCh, stdoutCh := make(chan interface{}, 100), make(chan interface{}, 100)

	for i, jb := range jobs {
		in := stdinCh
		out := stdoutCh
		wg.Add(1)

		//fmt.Printf("%v Calling %v function\n", time.Now().String(), i)
		go func(wg *sync.WaitGroup, jb job, in, out chan interface{}, i int) {
			defer wg.Done()
			jb(in, out)
			close(out)
			//fmt.Printf("%v Called %v\n", time.Now().String(), i)
		}(wg, jb, in, out, i)

		stdinCh = out
		stdoutCh = make(chan interface{}, 100)
	}
	wg.Wait()
}

func callDataSignerMd5(data string) string {
	muSignerMd5.Lock()
	defer muSignerMd5.Unlock()
	return DataSignerMd5(data)
}

func SingleHash(in, out chan interface{}) {
	hashWg := &sync.WaitGroup{}
	ii := 0
	//st := time.Now()
	//fmt.Println("SingleHash started: ", st)
	for data := range in {
		dataString := fmt.Sprintf("%v", data)
		//fmt.Printf("%v SingleHash data %v %v\n", time.Now().String(), dataString, data)
		ii++
		hashWg.Add(1)
		go func(data string) {
			defer hashWg.Done()
			md5ResultChannel := make(chan string, 0)
			go func(out chan<- string) {
				out <- DataSignerCrc32(callDataSignerMd5(dataString))
			}(md5ResultChannel)

			crc32Value := DataSignerCrc32(dataString)
			result := crc32Value + "~" + (<-md5ResultChannel)

			//fmt.Printf("%s SingleHash result %v %v in %v\n", time.Now().String(), dataString, result, time.Now().Sub(st))
			out <- result
		}(dataString)
	}

	//fmt.Println("Waiting for SingleHash... ", ii)
	hashWg.Wait()
	//fmt.Println("Waiting for SingleHash finished. Time: ", time.Now().Sub(st).String(), ii)
}
func MultiHash(in, out chan interface{}) {
	//0..5
	//crc32(th+data))
	//потом берёт конкатенацию результатов в порядке расчета (0..5), где data - то что пришло на вход (и ушло на выход из SingleHash)
	ths := make([]int, 6)
	ii := 0
	mHashWg := &sync.WaitGroup{}
	//st := time.Now()
	//fmt.Println("MultiHash started: ", st)
	for data := range in {
		//fmt.Println("MultiHash data in:", data)
		dataString := data.(string)
		ii++

		mHashWg.Add(1)
		go func(out chan<- interface{}, ii int) {
			var results = make([]string, len(ths))
			defer mHashWg.Done()

			loopWg := &sync.WaitGroup{}
			for i, _ := range ths {
				loopWg.Add(1)

				go func(i int, iterString string) {
					defer loopWg.Done()
					crc32 := DataSignerCrc32(iterString + dataString)
					//fmt.Printf("%v %v MultiHash: crc32(th+step%v)) %v %v\n", time.Now().String(), dataString, ii, iterString, crc32)
					results[i] = crc32
				}(i, strconv.Itoa(i))
			}

			loopWg.Wait()
			//fmt.Printf("%v MultiHash results: %v. in %v\n", time.Now().String(), strings.Join(results, "\n"), time.Now().Sub(st))
			out <- strings.Join(results, "")
		}(out, ii)

	}

	//fmt.Println("Waiting for MultiHash... ", ii)
	mHashWg.Wait()
	//fmt.Println("Waiting for MultiHash finished. Time: ", time.Now().Sub(st).String())
}

//получает все результаты,
// сортирует (https://golang.org/pkg/sort/),
// объединяет отсортированный результат через _ (символ подчеркивания) в одну строку
func CombineResults(in, out chan interface{}) {
	var results []string
	//st := time.Now()
	//fmt.Printf("%v CombineResults started\n", st.String())
	for hash := range in {
		//fmt.Printf("%v CombineResults: adding new hash: %v\n", time.Now().String(), hash)
		results = append(results, hash.(string))
	}
	sort.Strings(results)
	result := strings.Join(results, "_")
	//fmt.Printf("%v CombineResults finished in %v: %v %v\n", time.Now().String(), time.Now().Sub(st).String(), result, len(results))
	out <- result
}
