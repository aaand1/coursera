package main

import (
	ej "coursera/hw3_bench/easyjson"
	//"encoding/json"
	"fmt"
	"bufio"
	_ "github.com/mailru/easyjson"
	"io"
	//"io/ioutil"
//	_ "encoding/json"
	"os"
	"regexp"
	//"strings"
)

/*
BenchmarkSlow-8               50          32596890 ns/op        18958735 B/op     195857 allocs/op
BenchmarkFast-8              300           4618315 ns/op         4120901 B/op      11881 allocs/op

BenchmarkSlow-8               50          31375640 ns/op        18956353 B/op     195856 allocs/op
BenchmarkFast-8              300           4412351 ns/op         2615918 B/op      13851 allocs/op

//[]bytes
BenchmarkSlow-8               50          31553556 ns/op        18954968 B/op     195857 allocs/op
BenchmarkFast-8              300           3955625 ns/op         1389538 B/op      11842 allocs/op


//json.Unmarshal
BenchmarkSlow-8               50          32744260 ns/op        18948838 B/op     195854 allocs/op
BenchmarkFast-8              100          12952315 ns/op         2385464 B/op      56723 allocs/op

BenchmarkSolution-8 		 500 		   2782432 ns/op 		  559910 B/op 	   10422 allocs/op
*/

// вам надо написать более быструю оптимальную этой функции
func FastSearch(out io.Writer) {
	file, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}
	defer file.Close()
	
	reader := bufio.NewReader(file) 
    

	//fileContents, err := ioutil.ReadAll(file) //Stream? Yes, +mem. Require persistent read.
	//if err != nil {
	//	panic(err)
	//}

	r := regexp.MustCompile("@")
	reAndroid, reMSIE := regexp.MustCompile("Android"), regexp.MustCompile("MSIE")
	seenBrowsers := []string{}
	uniqueBrowsers := 0
	foundUsers := ""
	//lines := strings.Split(string(fileContents), "\n")

	//user := make(map[string]interface{}, 0) //Use strict type<-easyjson
	user := &ej.User{} //Use strict type<-easyjson
	//i := 0
	var line []byte
	for i := 0;;i++ { 
        line, err = reader.ReadBytes('\n') 
        if err != nil { 
            if err == io.EOF { 
                break
            } else { 
                panic(err)
            } 
        }
		
		//for i, line := range lines {
		// fmt.Printf("%v %v\n", err, line)
		//err = json.Unmarshal(line, &user)
	//}
	
	
		err = user.UnmarshalJSON(line) //Use easyjson for parsing?
		if err != nil {
			panic(err)
		}

		isAndroid := false
		isMSIE := false

		//browsers, ok := user["browsers"].([]interface{})
		//if !ok {
		//	// log.Println("cant cast browsers")
		//	continue
		//}

		for _, browser := range user.Browsers {
			//browser, ok := browserRaw.(string)
			//if !ok {
				// log.Println("cant cast browser to string")
				//continue
			//}

			if isCurrentAndroid, isCurrentMSIE := reAndroid.MatchString(browser), reMSIE.MatchString(browser);
				isCurrentAndroid || isCurrentMSIE {
				if isCurrentAndroid {
					isAndroid = true
				}
				if isCurrentMSIE {
					isMSIE = true
				}
				notSeenBefore := true
				for _, item := range seenBrowsers {
					if item == browser {
						notSeenBefore = false
					}
				}

				if notSeenBefore {
					// log.Printf("SLOW New browser: %s, first seen: %s", browser, user["name"])
					seenBrowsers = append(seenBrowsers, browser)
					uniqueBrowsers++
				}
			}
		}

		if !(isAndroid && isMSIE) {
			continue
		}

		//log.Println("FAST: Android and MSIE user:", user["name"], user["email"])
		email := r.ReplaceAllString(user.Email/*["email"].(string)*/, " [at] ")
		foundUsers += fmt.Sprintf("[%d] %s <%s>\n", i, user.Name/*["name"]*/, email)
	}

	fmt.Fprintln(out, "found users:\n"+foundUsers)
	fmt.Fprintln(out, "Total unique browsers", uniqueBrowsers/*len(uniqueBrowsers)*/)
}
