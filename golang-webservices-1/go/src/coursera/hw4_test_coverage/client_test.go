//Package main implements xml file based server and http integration tests for it.
//IT CONTAINS ONLY SUBMISSION TASK READY TEST CASES, which means that not all real cases are checked and covered with tests
package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"math"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"testing"
	"time"
)

type OrderField int

const (
	OrderById OrderField = iota
	OrderByAge
	OrderByName
)

func (f OrderField) String() string {
	return [...]string{"id", "age", "name"}[f]
}

func ParseOrderField(str string) (OrderField, error) {
	str = strings.ToLower(strings.TrimSpace(str))
	if str == "" {
		return OrderByName, nil
	}

	field, ok := map[string]OrderField{
		"id":   OrderById,
		"age":  OrderByAge,
		"name": OrderByName,
	}[str]

	if !ok {
		return OrderByName, fmt.Errorf(ErrorBadOrderField)
	}

	return field, nil
}

// код писать тут
type SearchUsersServer struct {
}

type SearchServerResponse struct {
	*SearchResponse
	//Users []User		`json:"users"`
}

type SearchServerRequest struct {
	*SearchRequest
	OrderField OrderField
}

type XmlUser struct {
	XMLName xml.Name `xml:"row"`

	Id        int    `xml:"id"`
	FirstName string `xml:"first_name"`
	LastName  string `xml:"last_name"`
	Age       int    `xml:"age"`
	About     string `xml:"about"`
	Gender    string `xml:"gender"`
}

func (xu *XmlUser) ToUser() *User {
	return &User{
		xu.Id, xu.FirstName + " " + xu.LastName,
		xu.Age, xu.About, xu.Gender,
	}
}

type XmlUsersList struct {
	XMLName xml.Name  `xml:"root"`
	Users   []XmlUser `xml:"row"`
}

func (s *SearchUsersServer) FilterUsers(users []XmlUser, req *SearchServerRequest) (result *SearchServerResponse, err error) {
	if _, err = ParseOrderField(req.OrderField.String()); err != nil {
		return
	}

	result = &SearchServerResponse{&SearchResponse{Users: []User{}, NextPage: false}}
	for _, u := range users {
		if req.Query == "" {
			result.Users = append(result.Users, *(u.ToUser()))
		} else if strings.Contains(u.FirstName, req.Query) || strings.Contains(u.LastName, req.Query) ||
			strings.Contains(u.About, req.Query) {
			result.Users = append(result.Users, *(u.ToUser()))
		}
	}

	if req.OrderBy != OrderByAsIs {
		sort.Slice(result.Users, func(i, j int) bool {
			switch req.OrderField {
			default:
				panic(fmt.Errorf("unknown users.OrderByField"))
			case OrderById:
				if req.OrderBy == OrderByAsc {
					return result.Users[i].Id > result.Users[j].Id
				}
				return result.Users[i].Id < result.Users[j].Id
			case OrderByAge:
				if req.OrderBy == OrderByAsc {
					return result.Users[i].Age > result.Users[j].Age
				}
				return result.Users[i].Age < result.Users[j].Age
			case OrderByName:
				if req.OrderBy == OrderByAsc {
					return result.Users[i].Name > result.Users[j].Name
				}
				return result.Users[i].Name < result.Users[j].Name
			}
		})
	}

	totalItems := len(result.Users)
	start := int(math.Min(float64(req.Offset), float64(totalItems)))
	end := int(math.Min(float64(totalItems), float64(req.Offset+req.Limit)))

	result.Users = result.Users[start:end]
	result.NextPage = totalItems > len(result.Users)
	return result, nil
}

func (s *SearchUsersServer) Search(req *SearchServerRequest) (*SearchServerResponse, error) {
	file, _ := os.Open("./dataset.xml")

	var xmlUsers XmlUsersList
	err := xml.NewDecoder(file).Decode(&xmlUsers)
	if err != nil {
		return nil, err
	}

	searchResponse, err := s.FilterUsers(xmlUsers.Users, req)
	if err != nil {
		return nil, err
	}

	return searchResponse, nil
}

func SearchServer(w http.ResponseWriter, req *http.Request) {
	if token := req.Header.Get("AccessToken"); token == "" {
		http.Error(w, fmt.Errorf("AccessToken wasn't passed").Error(), http.StatusUnauthorized)
	}

	query := req.URL.Query()
	searchReq := &SearchServerRequest{
		&SearchRequest{Limit: 20, Offset: 0, Query: "", OrderField: "Name", OrderBy: 0},
		OrderByName,
	}
	if val, ok := query["limit"]; ok {
		limit, err := strconv.Atoi(val[0])
		if err != nil {
			http.Error(w, fmt.Errorf("limit query param is invalid: %v",
				val).Error(), http.StatusBadRequest)
		}
		searchReq.Limit = int(math.Min(25, math.Max(0, float64(limit))))
	}
	if val, ok := query["offset"]; ok {
		offset, err := strconv.Atoi(val[0])
		if err != nil {
			http.Error(w, fmt.Errorf("offset query param is invalid: %v",
				val).Error(), http.StatusBadRequest)
		}
		searchReq.Offset = int(math.Max(0, float64(offset))) //warning: can be changed to support negative
	}
	if val, ok := query["query"]; ok && len(val) > 0 {
		searchReq.Query = val[0]
	}
	if val, ok := query["order_field"]; ok && len(val) > 0 {
		field := val[0]
		f, err := ParseOrderField(field)
		if err != nil {
			w.Header().Set("Content-Type", "application/json")
			_ = json.NewEncoder(w).Encode(SearchErrorResponse{ErrorBadOrderField})
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		searchReq.OrderField = f
	}
	if val, ok := query["order_by"]; ok && len(val) > 0 {
		order, err := strconv.Atoi(val[0])
		if err != nil {
			http.Error(w, fmt.Errorf("order_by query param is invalid: %v",
				val).Error(), http.StatusBadRequest)
		}
		searchReq.OrderBy = order
	}

	userServer := &SearchUsersServer{}
	res, err := userServer.Search(searchReq)
	if err != nil {
		http.Error(w, fmt.Errorf("Error: %v", err).Error(),
			http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Add("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(res.Users)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func TestSearchClient_FindUsers(t *testing.T) {
	type testCase struct {
		name string

		token             string
		limit, offset     int
		query, orderField string
		orderBy           int

		expectedError    error
		expectedResponse *SearchResponse
	}
	token := "mock_token"
	var cases []testCase
	invalidParamsCases := map[string]map[string]struct {
		values []interface{}
		adder  func(string, string, *[]testCase, ...interface{})
	}{
		"limit": {
			"limit must be > 0": {
				[]interface{}{-4, 0}, func(caseName, errName string, casesSlice *[]testCase, vals ...interface{}) {
					for _, val := range vals {
						*casesSlice = append(*casesSlice, testCase{
							name: fmt.Sprintf("%v:%v", caseName, val), limit: val.(int),
							expectedError: fmt.Errorf(errName), expectedResponse: &SearchResponse{
								Users: []User{},
							},
						})
					}
				},
			},
		},
		"offset": {
			"offset must be > 0": {
				[]interface{}{-4, -10}, func(caseName, errName string, casesSlice *[]testCase, vals ...interface{}) {
					for _, val := range vals {
						*casesSlice = append(*casesSlice, testCase{
							name: fmt.Sprintf("%v:%v", caseName, val), limit: 10, offset: val.(int),
							expectedError: fmt.Errorf(errName), expectedResponse: &SearchResponse{
								Users: []User{},
							},
						})
					}
				},
			},
		},
	}

	for paramName, paramErrors := range invalidParamsCases {
		for err, val := range paramErrors {
			caseName := fmt.Sprintf("It should return error '%s' for invalid %v param",
				err, paramName)
			val.adder(caseName, err, &cases, val.values...)
		}
	}

	users_Limit5_Offset1_OrderedBy_Name := []User{
		{19, "Bell Bauer", 26, `Nulla voluptate nostrud nostrud do ut tempor et quis non aliqua cillum in duis. Sit ipsum sit ut non proident exercitation. Quis consequat laboris deserunt adipisicing eiusmod non cillum magna.
`, "male"}, {22, "Beth Wynn", 31, `Proident non nisi dolore id non. Aliquip ex anim cupidatat dolore amet veniam tempor non adipisicing. Aliqua adipisicing eu esse quis reprehenderit est irure cillum duis dolor ex. Laborum do aute commodo amet. Fugiat aute in excepteur ut aliqua sint fugiat do nostrud voluptate duis do deserunt. Elit esse ipsum duis ipsum.
`, "female"}, {5, "Beulah Stark", 30, `Enim cillum eu cillum velit labore. In sint esse nulla occaecat voluptate pariatur aliqua aliqua non officia nulla aliqua. Fugiat nostrud irure officia minim cupidatat laborum ad incididunt dolore. Fugiat nostrud eiusmod ex ea nulla commodo. Reprehenderit sint qui anim non ad id adipisicing qui officia Lorem.
`, "female"}, {0, "Boyd Wolf", 22, `Nulla cillum enim voluptate consequat laborum esse excepteur occaecat commodo nostrud excepteur ut cupidatat. Occaecat minim incididunt ut proident ad sint nostrud ad laborum sint pariatur. Ut nulla commodo dolore officia. Consequat anim eiusmod amet commodo eiusmod deserunt culpa. Ea sit dolore nostrud cillum proident nisi mollit est Lorem pariatur. Lorem aute officia deserunt dolor nisi aliqua consequat nulla nostrud ipsum irure id deserunt dolore. Minim reprehenderit nulla exercitation labore ipsum.
`, "male"}, {2, "Brooks Aguilar", 25, `Velit ullamco est aliqua voluptate nisi do. Voluptate magna anim qui cillum aliqua sint veniam reprehenderit consectetur enim. Laborum dolore ut eiusmod ipsum ad anim est do tempor culpa ad do tempor. Nulla id aliqua dolore dolore adipisicing.
`, "male"},
	}

	users_Limit12_Offset2_orderedBy_Age := []User{
		{2, "Brooks Aguilar", 25, `Velit ullamco est aliqua voluptate nisi do. Voluptate magna anim qui cillum aliqua sint veniam reprehenderit consectetur enim. Laborum dolore ut eiusmod ipsum ad anim est do tempor culpa ad do tempor. Nulla id aliqua dolore dolore adipisicing.
`, "male"},
		{27, "Rebekah Sutton", 26, `Aliqua exercitation ad nostrud et exercitation amet quis cupidatat esse nostrud proident. Ullamco voluptate ex minim consectetur ea cupidatat in mollit reprehenderit voluptate labore sint laboris. Minim cillum et incididunt pariatur amet do esse. Amet irure elit deserunt quis culpa ut deserunt minim proident cupidatat nisi consequat ipsum.
`, "female"},
		{19, "Bell Bauer", 26, `Nulla voluptate nostrud nostrud do ut tempor et quis non aliqua cillum in duis. Sit ipsum sit ut non proident exercitation. Quis consequat laboris deserunt adipisicing eiusmod non cillum magna.
`, "male"},
		{3, "Everett Dillard", 27, `Sint eu id sint irure officia amet cillum. Amet consectetur enim mollit culpa laborum ipsum adipisicing est laboris. Adipisicing fugiat esse dolore aliquip quis laborum aliquip dolore. Pariatur do elit eu nostrud occaecat.
`, "male"},
		{18, "Terrell Hall", 27, `Ut nostrud est est elit incididunt consequat sunt ut aliqua sunt sunt. Quis consectetur amet occaecat nostrud duis. Fugiat in irure consequat laborum ipsum tempor non deserunt laboris id ullamco cupidatat sit. Officia cupidatat aliqua veniam et ipsum labore eu do aliquip elit cillum. Labore culpa exercitation sint sint.
`, "male"},
		{8, "Glenn Jordan", 29, `Duis reprehenderit sit velit exercitation non aliqua magna quis ad excepteur anim. Eu cillum cupidatat sit magna cillum irure occaecat sunt officia officia deserunt irure. Cupidatat dolor cupidatat ipsum minim consequat Lorem adipisicing. Labore fugiat cupidatat nostrud voluptate ea eu pariatur non. Ipsum quis occaecat irure amet esse eu fugiat deserunt incididunt Lorem esse duis occaecat mollit.
`, "male"},
		{10, "Henderson Maxwell", 30, `Ex et excepteur anim in eiusmod. Cupidatat sunt aliquip exercitation velit minim aliqua ad ipsum cillum dolor do sit dolore cillum. Exercitation eu in ex qui voluptate fugiat amet.
`, "male"},
		{5, "Beulah Stark", 30, `Enim cillum eu cillum velit labore. In sint esse nulla occaecat voluptate pariatur aliqua aliqua non officia nulla aliqua. Fugiat nostrud irure officia minim cupidatat laborum ad incididunt dolore. Fugiat nostrud eiusmod ex ea nulla commodo. Reprehenderit sint qui anim non ad id adipisicing qui officia Lorem.
`, "female"},
		{22, "Beth Wynn", 31, `Proident non nisi dolore id non. Aliquip ex anim cupidatat dolore amet veniam tempor non adipisicing. Aliqua adipisicing eu esse quis reprehenderit est irure cillum duis dolor ex. Laborum do aute commodo amet. Fugiat aute in excepteur ut aliqua sint fugiat do nostrud voluptate duis do deserunt. Elit esse ipsum duis ipsum.
`, "female"},
		{24, "Gonzalez Anderson", 33, `Quis consequat incididunt in ex deserunt minim aliqua ea duis. Culpa nisi excepteur sint est fugiat cupidatat nulla magna do id dolore laboris. Aute cillum eiusmod do amet dolore labore commodo do pariatur sit id. Do irure eiusmod reprehenderit non in duis sunt ex. Labore commodo labore pariatur ex minim qui sit elit.
`, "male"},
		{34, "Kane Sharp", 34, `Lorem proident sint minim anim commodo cillum. Eiusmod velit culpa commodo anim consectetur consectetur sint sint labore. Mollit consequat consectetur magna nulla veniam commodo eu ut et. Ut adipisicing qui ex consectetur officia sint ut fugiat ex velit cupidatat fugiat nisi non. Dolor minim mollit aliquip veniam nostrud. Magna eu aliqua Lorem aliquip.
`, "male"},
		{33, "Twila Snow", 36, `Sint non sunt adipisicing sit laborum cillum magna nisi exercitation. Dolore officia esse dolore officia ea adipisicing amet ea nostrud elit cupidatat laboris. Proident culpa ullamco aute incididunt aute. Laboris et nulla incididunt consequat pariatur enim dolor incididunt adipisicing enim fugiat tempor ullamco. Amet est ullamco officia consectetur cupidatat non sunt laborum nisi in ex. Quis labore quis ipsum est nisi ex officia reprehenderit ad adipisicing fugiat. Labore fugiat ea dolore exercitation sint duis aliqua.
`, "female"},
	}
	users_LImit10_offset4 := []User{
		{8, "Glenn Jordan", 29, `Duis reprehenderit sit velit exercitation non aliqua magna quis ad excepteur anim. Eu cillum cupidatat sit magna cillum irure occaecat sunt officia officia deserunt irure. Cupidatat dolor cupidatat ipsum minim consequat Lorem adipisicing. Labore fugiat cupidatat nostrud voluptate ea eu pariatur non. Ipsum quis occaecat irure amet esse eu fugiat deserunt incididunt Lorem esse duis occaecat mollit.
`, "male"}, {10, "Henderson Maxwell", 30, `Ex et excepteur anim in eiusmod. Cupidatat sunt aliquip exercitation velit minim aliqua ad ipsum cillum dolor do sit dolore cillum. Exercitation eu in ex qui voluptate fugiat amet.
`, "male"}, {13, "Whitley Davidson", 40, `Consectetur dolore anim veniam aliqua deserunt officia eu. Et ullamco commodo ad officia duis ex incididunt proident consequat nostrud proident quis tempor. Sunt magna ad excepteur eu sint aliqua eiusmod deserunt proident. Do labore est dolore voluptate ullamco est dolore excepteur magna duis quis. Quis laborum deserunt ipsum velit occaecat est laborum enim aute. Officia dolore sit voluptate quis mollit veniam. Laborum nisi ullamco nisi sit nulla cillum et id nisi.
`, "male"}, {15, "Allison Valdez", 21, `Labore excepteur voluptate velit occaecat est nisi minim. Laborum ea et irure nostrud enim sit incididunt reprehenderit id est nostrud eu. Ullamco sint nisi voluptate cillum nostrud aliquip et minim. Enim duis esse do aute qui officia ipsum ut occaecat deserunt. Pariatur pariatur nisi do ad dolore reprehenderit et et enim esse dolor qui. Excepteur ullamco adipisicing qui adipisicing tempor minim aliquip.
`, "male"}, {18, "Terrell Hall", 27, `Ut nostrud est est elit incididunt consequat sunt ut aliqua sunt sunt. Quis consectetur amet occaecat nostrud duis. Fugiat in irure consequat laborum ipsum tempor non deserunt laboris id ullamco cupidatat sit. Officia cupidatat aliqua veniam et ipsum labore eu do aliquip elit cillum. Labore culpa exercitation sint sint.
`, "male"}, {19, "Bell Bauer", 26, `Nulla voluptate nostrud nostrud do ut tempor et quis non aliqua cillum in duis. Sit ipsum sit ut non proident exercitation. Quis consequat laboris deserunt adipisicing eiusmod non cillum magna.
`, "male"}, {22, "Beth Wynn", 31, `Proident non nisi dolore id non. Aliquip ex anim cupidatat dolore amet veniam tempor non adipisicing. Aliqua adipisicing eu esse quis reprehenderit est irure cillum duis dolor ex. Laborum do aute commodo amet. Fugiat aute in excepteur ut aliqua sint fugiat do nostrud voluptate duis do deserunt. Elit esse ipsum duis ipsum.
`, "female"}, {24, "Gonzalez Anderson", 33, `Quis consequat incididunt in ex deserunt minim aliqua ea duis. Culpa nisi excepteur sint est fugiat cupidatat nulla magna do id dolore laboris. Aute cillum eiusmod do amet dolore labore commodo do pariatur sit id. Do irure eiusmod reprehenderit non in duis sunt ex. Labore commodo labore pariatur ex minim qui sit elit.
`, "male"}, {26, "Sims Cotton", 39, `Ex cupidatat est velit consequat ad. Tempor non cillum labore non voluptate. Et proident culpa labore deserunt ut aliquip commodo laborum nostrud. Anim minim occaecat est est minim.
`, "male"}, {27, "Rebekah Sutton", 26, `Aliqua exercitation ad nostrud et exercitation amet quis cupidatat esse nostrud proident. Ullamco voluptate ex minim consectetur ea cupidatat in mollit reprehenderit voluptate labore sint laboris. Minim cillum et incididunt pariatur amet do esse. Amet irure elit deserunt quis culpa ut deserunt minim proident cupidatat nisi consequat ipsum.
`, "female"},
	}
	users_Limit5_offset1_orderBy_Id := []User{
		{2, "Brooks Aguilar", 25, `Velit ullamco est aliqua voluptate nisi do. Voluptate magna anim qui cillum aliqua sint veniam reprehenderit consectetur enim. Laborum dolore ut eiusmod ipsum ad anim est do tempor culpa ad do tempor. Nulla id aliqua dolore dolore adipisicing.
`, "male"}, {3, "Everett Dillard", 27, `Sint eu id sint irure officia amet cillum. Amet consectetur enim mollit culpa laborum ipsum adipisicing est laboris. Adipisicing fugiat esse dolore aliquip quis laborum aliquip dolore. Pariatur do elit eu nostrud occaecat.
`, "male"}, {5, "Beulah Stark", 30, `Enim cillum eu cillum velit labore. In sint esse nulla occaecat voluptate pariatur aliqua aliqua non officia nulla aliqua. Fugiat nostrud irure officia minim cupidatat laborum ad incididunt dolore. Fugiat nostrud eiusmod ex ea nulla commodo. Reprehenderit sint qui anim non ad id adipisicing qui officia Lorem.
`, "female"}, {8, "Glenn Jordan", 29, `Duis reprehenderit sit velit exercitation non aliqua magna quis ad excepteur anim. Eu cillum cupidatat sit magna cillum irure occaecat sunt officia officia deserunt irure. Cupidatat dolor cupidatat ipsum minim consequat Lorem adipisicing. Labore fugiat cupidatat nostrud voluptate ea eu pariatur non. Ipsum quis occaecat irure amet esse eu fugiat deserunt incididunt Lorem esse duis occaecat mollit.
`, "male"}, {10, "Henderson Maxwell", 30, `Ex et excepteur anim in eiusmod. Cupidatat sunt aliquip exercitation velit minim aliqua ad ipsum cillum dolor do sit dolore cillum. Exercitation eu in ex qui voluptate fugiat amet.
`, "male"},
	}
	cases = append(cases, []testCase{
		{"It should perform search request successfully", token,
			1, 0, "Hilda", "age", -1,
			nil, &SearchResponse{Users: []User{
				{1, "Hilda Mayer", 21, `Sit commodo consectetur minim amet ex. Elit aute mollit fugiat labore sint ipsum dolor cupidatat qui reprehenderit. Eu nisi in exercitation culpa sint aliqua nulla nulla proident eu. Nisi reprehenderit anim cupidatat dolor incididunt laboris mollit magna commodo ex. Cupidatat sit id aliqua amet nisi et voluptate voluptate commodo ex eiusmod et nulla velit.
`, "female"},
			}}}, {
			name: "It should limit & shift response successfully", token: token,
			limit: 10, offset: 4, query: "cillum",
			expectedError: nil, expectedResponse: &SearchResponse{Users: users_LImit10_offset4, NextPage: true},
		}, {
			"It should order response successfully by age", token,
			12, 2, "cillum", "Age", 1,
			nil, &SearchResponse{Users: users_Limit12_Offset2_orderedBy_Age, NextPage: true},
		}, {
			"It should order response successfully by name", token,
			5, 1, "cillum", "", 1,
			nil, &SearchResponse{Users: users_Limit5_Offset1_OrderedBy_Name, NextPage: true},
		}, {
			"It should order response successfully by name", token,
			5, 1, "cillum", "name", 1,
			nil, &SearchResponse{Users: users_Limit5_Offset1_OrderedBy_Name, NextPage: true},
		}, {
			"It should order response successfully by id", token,
			5, 1, "cillum", "id", 1,
			nil, &SearchResponse{Users: users_Limit5_offset1_orderBy_Id, NextPage: true},
		}, {
			"It should calculate NextPage correctly", token,
			1, 0, "Unknown_Query", "name", 1,
			nil, &SearchResponse{Users: []User{}, NextPage: false},
		}, {
			name:          "It should require AccessToken",
			token:         "",
			limit:         1,
			expectedError: fmt.Errorf("Bad AccessToken"), expectedResponse: nil,
		},
	}...)

	mockServer := httptest.NewServer(http.HandlerFunc(SearchServer))
	defer mockServer.Close()

	for _, item := range cases {
		t.Run(item.name, func(t *testing.T) {
			client := SearchClient{item.token, mockServer.URL}
			searchReq := SearchRequest{item.limit, item.offset, item.query, item.orderField, item.orderBy}
			res, err := client.FindUsers(searchReq)

			if !reflect.DeepEqual(item.expectedError, err) {
				t.Fatalf("expected error %v, but got %v", item.expectedError, err)
			}

			if res != nil && !reflect.DeepEqual(*item.expectedResponse, *res) {
				t.Fatalf("expected item.expectedResponse to equal %v, but got %v", *item.expectedResponse, *res)
			}
		})
	}

	t.Run("It should allow maximum 25 users per query", func(t *testing.T) {
		client := SearchClient{"mock_token", mockServer.URL}
		searchReq := SearchRequest{Limit: 30}
		res, err := client.FindUsers(searchReq)

		if err != nil {
			t.Fatalf("expected error nil, but got %v", err)
		}
		if len(res.Users) != 25 {
			t.Fatalf("expected SearchRequest{Limit: 30} response to be normalized to 25, but got %v", len(res.Users))
		}
	})
}

func TestSearchClient_FindUsers_ErrorCodes(t *testing.T) {
	type testCase struct {
		name string

		token      string
		orderField string

		reqUrl             *url.URL
		responseHttpStatus int
		responseBody       []byte
		expectedError      error
	}

	invalidReqURL, _ := url.Parse("xyz://foo.bar.com")

	cases := []testCase{{
		name:               "It should return ErrorBadOrderField for invalid order field",
		orderField:         "unknown",
		expectedError:      fmt.Errorf("OrderFeld unknown invalid"),
		responseHttpStatus: http.StatusBadRequest,
		responseBody:       []byte("{\"Error\": \"ErrorBadOrderField\"}"),
	}, {
		name:               "It should return 'unknown bad request error'",
		orderField:         "unknown2",
		expectedError:      fmt.Errorf("unknown bad request error: ErrorBadRequest"),
		responseHttpStatus: http.StatusBadRequest,
		responseBody:       []byte("{\"Error\": \"ErrorBadRequest\"}"),
	}, {
		name:               "It should return SearchServer fatal error for 500/InternalServerError",
		orderField:         "id",
		expectedError:      fmt.Errorf("SearchServer fatal error"),
		responseHttpStatus: http.StatusInternalServerError,
	}, {
		name:               "It should return can't unpack result json",
		expectedError:      fmt.Errorf("cant unpack error json: %s", "invalid character ':' after top-level value"),
		responseHttpStatus: http.StatusBadRequest,
		responseBody:       []byte("\"Id\": \"1\", \"Age\": 13}"),
	}, {
		name:               "It should timeout after 1s and return timeout error",
		responseHttpStatus: http.StatusGatewayTimeout,
		expectedError:      fmt.Errorf("timeout for %s", "limit=11&offset=0&order_by=0&order_field=&query="),
	}, {
		name:               "It should return unknown error for client.Do request",
		reqUrl:             invalidReqURL,
		responseHttpStatus: http.StatusInternalServerError,
		expectedError: fmt.Errorf("unknown error %s",
			fmt.Sprintf("Get %s?limit=11&offset=0&order_by=0&order_field=&query=: unsupported protocol scheme \"%s\"",
				invalidReqURL.String(), invalidReqURL.Scheme)),
	}, {
		name:               "It should return json unpack error for invalid json in body",
		responseHttpStatus: http.StatusOK,
		responseBody:       []byte(""),
		expectedError:      fmt.Errorf("cant unpack result json: %s", "unexpected end of JSON input"),
	},
	}

	type testRequestsProcessor struct {
		current *testCase
	}

	withCase := func(trp *testRequestsProcessor, tc *testCase) {
		trp.current = tc
	}
	buildHandleFunc := func(rp *testRequestsProcessor) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			tc := rp.current
			if tc.responseHttpStatus != 0 {
				w.WriteHeader(tc.responseHttpStatus)
				if tc.responseHttpStatus == http.StatusGatewayTimeout {
					time.Sleep(time.Second + 200*time.Millisecond)
				}
				if tc.responseBody != nil {
					w.Header().Add("Content-Type", "application/json")
					w.Write(tc.responseBody)
				}
				return
			}
			SearchServer(w, r)
		}
	}

	processor := &testRequestsProcessor{}
	mockServer := httptest.NewServer(http.HandlerFunc(buildHandleFunc(processor)))
	defer mockServer.Close()

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			withCase(processor, &tc)
			serverUrl := mockServer.URL
			if tc.reqUrl != nil {
				serverUrl = tc.reqUrl.String()
			}
			client := SearchClient{"mock_token", serverUrl}
			searchReq := SearchRequest{Limit: 10, OrderField: tc.orderField}
			_, err := client.FindUsers(searchReq)

			if !reflect.DeepEqual(tc.expectedError, err) {
				t.Fatalf("expected error %v, but got %v", tc.expectedError, err)
			}
		})
	}
}
