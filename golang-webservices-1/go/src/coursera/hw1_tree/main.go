package main

import (
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strings"
	//"path/filepath"
	//"strings"
)

func main() {
	out := os.Stdout
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(out, path, printFiles)
	if err != nil {
		panic(err.Error())
	}
}

func dirTree(out io.Writer, path string, printFiles bool) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()
	fileAbsPath, aperr := filepath.Abs(path)
	if aperr != nil {
		return aperr
	}

	isInLastNode := false
	lastParentLevel := -1
	perr := dirTreePrint(out, printFiles, lastParentLevel, 0, isInLastNode, file, fileAbsPath)
	if perr != nil {
		return perr
	}
	return nil
}

type ByNameSorter []os.FileInfo

func (bn ByNameSorter) Len() int {
	return len(bn)
}

func (bn ByNameSorter) Less(i, j int) bool {
	return bn[i].Name() < bn[j].Name()
}

func (bn ByNameSorter) Swap(i, j int) {
	bn[i], bn[j] = bn[j], bn[i]
}

type IFilter interface {
	Filter([]interface{}) []interface{}
}

type ByTypeFilter struct {
	/*state, conditions*/
}

func (f *ByTypeFilter) Filter(files []os.FileInfo) (result []os.FileInfo) {
	//typedFiles := []*os.FileInfo(files)
	return filterByType(files)
}

func filterByType(files []os.FileInfo) (result []os.FileInfo) {
	result = []os.FileInfo{}
	for _, fi := range files {
		if !fi.IsDir() {
			continue
		}
		result = append(result, fi)
	}
	return
}

var (
	filter = /*IFilter*/ &ByTypeFilter{}
)

func dirTreePrint(out io.Writer, printFiles bool, lastParentLevel, level int, isInLastNode bool, file *os.File, fileAbsPath string) error {
	files, err := file.Readdir(-1)
	if err != nil {
		return err
	}

	if !printFiles {
		files = filter.Filter(files)
		//files = filterByType(files)
	}
	sort.Sort(ByNameSorter(files))

	lenFiles := len(files)

	//if lenFiles == 0 {
	//	fmt.Fprintf(out, "(empty)")
	//	return nil
	//}

	if level == 0 && lenFiles == 1 {
		isInLastNode = true
		lastParentLevel = level
	}

	for i, fInfo := range files {
		isLast := i == lenFiles-1
		if isLast {
			//lastParentLevel = level - 1
			//if lastParentLevel ==
		}

		prefixStr := getPrefixGlyph(&fInfo, lastParentLevel, level, isInLastNode, isLast)

		size := fInfo.Size()
		sizeString := ""
		if printFiles && !fInfo.IsDir() {
			sizeString = fmt.Sprintf("(%vb)", size)
			if size == 0 {
				sizeString = "(empty)"
			}
		}

		if !fInfo.IsDir() {

			fmt.Fprintf(out, "%s%s %v\n", prefixStr, fInfo.Name(), sizeString)

		} else {
			fmt.Fprintf(out, "%s%s\n", prefixStr, fInfo.Name())

			absPath := path.Join(fileAbsPath, string(os.PathSeparator), fInfo.Name())

			fFile, err := os.Open(absPath)
			if err != nil {
				fmt.Fprintf(out, "(error):%s", err.Error())
				continue
			}

			if fFile != nil {
				newLastParentLevel := lastParentLevel
				newIsInLastNode := isInLastNode
				if isLast {
					if lastParentLevel > -1 {
						newLastParentLevel = lastParentLevel
					} else {
						newLastParentLevel = level
					}

					newIsInLastNode = true
				}
				derr := dirTreePrint(out, printFiles, newLastParentLevel, level+1, newIsInLastNode, fFile, absPath)
				if derr != nil {
					fmt.Fprintf(out, "%s%s err: %s", prefixStr, fInfo.Name(), err)
				}
			}
		}
	}
	return nil
}

func getPrefixGlyph(fi *os.FileInfo, lastParentLevel, level int, isInLastNode, isLast bool) (prefixStr string) {
	glyph := "├───"
	if isLast {
		glyph = "└───"
	}

	prefixStr = ""
	if level == 0 {
		prefixStr = glyph
		return
	}

	if !isInLastNode {
		prefixStr = strings.Repeat("│\t", level) + glyph
	} else {
		//Aka >mkdir -p ./testdata/zline/lorem/gopsum{dolor.txt,gopher.png} non-finished fix
		//if !isLast {
		//	prefixStr = strings.Repeat("|\t", lastParentLevel) +
		//		strings.Repeat("\t", (level))
		//}
		prefixStr = strings.Repeat("│\t", lastParentLevel) +
			strings.Repeat("\t", (level-lastParentLevel)) +
			glyph
	}

	return
}
